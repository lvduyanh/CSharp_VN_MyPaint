﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPaint
{
    public partial class Form1 : Form
    {
        private ArrayList arr;  // buttons array
        private int curSize;    // pnDraw current size
        private paintMode currMode;   // current mode

        // paDraw's location
        private int rootX;
        private int rootY;
        // color
        private Color clRight;
        private Color clLeft;
        // mouse click
        private bool isClick;
        // pointers
        private Point p1;
        private Point p2;
        // for draw
        Bitmap bm;
        Graphics gps;
        Pen pen;
        // for eraser
        SolidBrush sb;
        Size eraser;
        public Form1()
        {
            InitializeComponent();
            this.Width = 976;
            this.Height = 650;  // make pbxDraw size to 960:540 (16:9)
            //this.SetStyle(ControlStyles.UserPaint, true);
            //this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            //this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.DoubleBuffered = true;

            arr = new ArrayList();
            arr.Add(btnPencil);
            arr.Add(btnEraser);
            arr.Add(btnLine);
            arr.Add(btnRectangle);
            arr.Add(btnEllipse);

            currMode = paintMode.PENCIL;
            rootX = pbxDraw.Location.X;
            rootY = pbxDraw.Location.Y;
            clLeft = Color.Black;
            clRight = Color.White;
            isClick = false;
            p1 = new Point();
            p2 = new Point();

            bm = new Bitmap(pbxDraw.Width, pbxDraw.Height);
            gps = Graphics.FromImage(bm);
            pen = new Pen(clLeft);
            sb = new SolidBrush(clRight);
            eraser = new Size(20, 20);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                cbbSize.Items.Add(i);
            }
            cbbSize.SelectedIndex = 0;
            curSize = (int)cbbSize.SelectedItem;

            sttLblSize.Text = getDrawSize();

            // timer for refesh pbxDraw
            timer1.Interval = 1000 / 60;    // 60 FPS
            timer1.Enabled = true;
        }

        private string getDrawSize()
        {
            return "Size: " + pbxDraw.Size.Width + " x " + pbxDraw.Size.Height + "px";
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            sttLblSize.Text = getDrawSize();
            if (pbxDraw.Width >= 100 && pbxDraw.Height >= 100)
            {
                Bitmap bmTmp = bm;
                bm = new Bitmap(pbxDraw.Width, pbxDraw.Height);
                gps = Graphics.FromImage(bm);
                gps.DrawImage(bmTmp, 0, 0);
                pbxDraw.Refresh();
            }
        }

        private void btnPencil_Click(object sender, EventArgs e)
        {
            currMode = paintMode.PENCIL;
            switchCheckState((ToolStripButton)sender);
        }

        private void btnEraser_Click(object sender, EventArgs e)
        {
            currMode = paintMode.ERASER;
            switchCheckState((ToolStripButton)sender);
        }

        private void btnLine_Click(object sender, EventArgs e)
        {
            currMode = paintMode.LINE;
            switchCheckState((ToolStripButton)sender);
        }

        private void btnRectangle_Click(object sender, EventArgs e)
        {
            currMode = paintMode.RECTANGLE;
            switchCheckState((ToolStripButton)sender);
        }

        private void btnEllipse_Click(object sender, EventArgs e)
        {
            currMode = paintMode.ELLIPSE;
            switchCheckState((ToolStripButton)sender);
        }

        private void btnLeftColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = clLeft;
            if (cd.ShowDialog() == DialogResult.OK)
            {
                clLeft = cd.Color;
                btnLeftColor.BackColor = cd.Color;
            }
        }

        private void btnRightColor_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = clRight;
            if (cd.ShowDialog() == DialogResult.OK)
            {
                clRight = cd.Color;
                btnRightColor.BackColor = cd.Color;
            }
        }

        private void switchCheckState(ToolStripButton btn)
        {
            foreach (ToolStripButton tsb in arr)
            {
                if (tsb != btn)
                {
                    tsb.CheckState = CheckState.Unchecked;
                }
            }
            btn.CheckState = CheckState.Checked;
            pbxDraw.Refresh();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pbxDraw_MouseDown(object sender, MouseEventArgs e)
        {
            isClick = true;
            p1 = e.Location;
            if (e.Button == MouseButtons.Left)
            {
                pen.Color = clLeft;
            }
            else
            {
                pen.Color = clRight;
            }
        }

        private void pbxDraw_MouseMove(object sender, MouseEventArgs e)
        {
            sttLblLocal.Text = e.Location.X + ", " + e.Location.Y + "px";
            if (isClick)
            {
                p2 = e.Location;
                switch (currMode)
                {
                    case paintMode.PENCIL:
                        gps.DrawLine(pen, p1, p2);
                        p1 = p2;
                        break;
                    case paintMode.ERASER:
                        sb.Color = clRight;
                        gps.FillRectangle(sb, p2.X - eraser.Width/2, p2.Y - eraser.Height/2, 
                            eraser.Width, eraser.Height);
                        break;
                }
            }
            else
            {
                if (currMode == paintMode.ERASER)
                {
                    p2 = e.Location;
                }
            }
        }

        private void pbxDraw_MouseUp(object sender, MouseEventArgs e)
        {
            isClick = false;
            int dx = p2.X - p1.X;
            int dy = p2.Y - p1.Y;
            switch (currMode)
            {
                case paintMode.PENCIL:
                    break;
                case paintMode.ERASER:
                    break;
                case paintMode.LINE:
                    gps.DrawLine(pen, p1, p2);
                    break;
                case paintMode.RECTANGLE:
                    if (dx < 0 && dy < 0)
                    {
                        gps.DrawRectangle(pen, p2.X, p2.Y, -dx, -dy);
                    }
                    else if (dx < 0)
                    {
                        gps.DrawRectangle(pen, p2.X, p1.Y, -dx, dy);
                    }
                    else if (dy < 0)
                    {
                        gps.DrawRectangle(pen, p1.X, p2.Y, dx, -dy);
                    }
                    else
                    {
                        gps.DrawRectangle(pen, p1.X, p1.Y, dx, dy);
                    }
                    break;
                case paintMode.ELLIPSE:
                    gps.DrawEllipse(pen, p1.X, p1.Y, dx, dy);
                    break;
                default:
                    currMode = paintMode.PENCIL;
                    break;
            }
        }

        private void pbxDraw_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(bm, 0, 0);
            if (isClick)
            {
                int dx = p2.X - p1.X;
                int dy = p2.Y - p1.Y;
                switch (currMode)
                {
                    case paintMode.PENCIL:
                        break;
                    case paintMode.ERASER:
                        break;
                    case paintMode.LINE:
                        e.Graphics.DrawLine(pen, p1, p2);
                        break;
                    case paintMode.RECTANGLE:
                        if (dx < 0 && dy < 0)
                        {
                            e.Graphics.DrawRectangle(pen, p2.X, p2.Y, -dx, -dy);
                        }
                        else if (dx < 0)
                        {
                            e.Graphics.DrawRectangle(pen, p2.X, p1.Y, -dx, dy);
                        }
                        else if (dy < 0)
                        {
                            e.Graphics.DrawRectangle(pen, p1.X, p2.Y, dx, -dy);
                        }
                        else
                        {
                            e.Graphics.DrawRectangle(pen, p1.X, p1.Y, dx, dy);
                        }
                        break;
                    case paintMode.ELLIPSE:
                        e.Graphics.DrawEllipse(pen, p1.X, p1.Y, dx, dy);
                        break;
                    default:
                        currMode = paintMode.PENCIL;
                        break;
                }
            }
            if (currMode == paintMode.ERASER)
            {
                e.Graphics.DrawRectangle(pen, p2.X - eraser.Width / 2, p2.Y - eraser.Height / 2,
                            eraser.Width, eraser.Height);
            }
        }

        private void cbbSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            curSize = (int)cbbSize.SelectedItem;
            pen.Width = curSize;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bm = new Bitmap(pbxDraw.Width, pbxDraw.Height);
            gps = Graphics.FromImage(bm);
            pbxDraw.Refresh();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Pictures (*.jpg;*.jpeg;*.png;*bmp)|*.jpg;*.jpeg;*.png;*bmp";
            ofd.ValidateNames = true;
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(ofd.FileName);
                //Rectangle rec = new Rectangle(0, 0, bm.Width, bm.Height);
                gps.DrawImage(img, 0, 0);
                pbxDraw.Refresh();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "*.jpg|*.jpg|*.png|*.png|*.bmp|*.bmp";
            sfd.ValidateNames = true;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                bm.Save(sfd.FileName);
            }
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("My Pain\nVersion: 1.0\nCode by @lvduyanh", "About");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isClick || currMode == paintMode.ERASER)
            {
                pbxDraw.Refresh();
            }
        }
    }
    public enum paintMode
    {
        PENCIL,
        ERASER,
        LINE,
        RECTANGLE,
        ELLIPSE
    }
}
